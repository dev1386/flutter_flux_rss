import 'package:intl/intl.dart';
class DateFormatter {
  String formattedDate(String string) {
    String format = 'EEE, dd MMM yyyy HH:mm:ss Z';
    String response = "il y a ";
    var formatter = DateFormat(format);
    DateTime dateTime = formatter.parse(string);
    if(dateTime== null) {
      return "Date inconnue";
    } else {
      var difference = DateTime.now().difference(dateTime);
      var days = difference.inDays;
      var min = difference.inMinutes;
      var hours = difference.inHours;
      if(days > 0) {
        response += '$days jour(s)';
      } else if(hours > 0) {
        response += '$hours heure(s)';
      } else if(min > 0){
        response += '$min minute(s)';
      } else {
        response += '${difference.inSeconds} secondes';
      }

      return response;

    }
  }
}