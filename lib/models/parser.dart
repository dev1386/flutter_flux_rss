import 'package:http/http.dart';
import 'package:webfeed/webfeed.dart';
class Parser {
  final url = 'http://www.france24.com/fr/rss';
  Future<RssFeed> loadRss() async {
    final response = await get(url);
    if(response.statusCode == 200) {
      final feed = RssFeed.parse(response.body);
      return feed;
    } else {
      print('erreur : ${response.statusCode}');
    }
  }
}