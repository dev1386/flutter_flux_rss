import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluxrss/models/date_formateur.dart';
import 'package:fluxrss/widgets/page_details.dart';
import 'package:webfeed/domain/rss_feed.dart';

import 'customText.dart';

class CustomGrid extends StatefulWidget{
  RssFeed feed;

  CustomGrid(this.feed);

  @override
  _CustomGridState createState() => new _CustomGridState();
}

class _CustomGridState extends State<CustomGrid> {
  @override
  Widget build(BuildContext context) {
    return new GridView.builder(
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemCount: widget.feed.items.length,
      itemBuilder: (context, i) {
        var item = widget.feed.items[i];
        return new Card(
          elevation: 10.0,
          child: new InkWell(
            onTap: () {
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) {
                    return new DetailsPage(item);
                  },
                ),
              );
            },
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    new CustomText(
                      (item.dc.creator != null) ? item.dc.creator : ' ',
                      fontSize: 12.0,
                    ),
                    new CustomText(
                      new DateFormatter().formattedDate(item.pubDate),
                      color: Colors.red,
                    ),
                  ],
                ),
                new CustomText(item.title),
                new Card(
                  elevation: 7.5,
                  child: new Container(
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: new Image.network(
                      item.enclosure.url,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

}