import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluxrss/models/date_formateur.dart';
import 'package:fluxrss/widgets/customText.dart';
import 'package:fluxrss/widgets/page_details.dart';
import 'package:webfeed/webfeed.dart';

class CustomList extends StatefulWidget {

  RssFeed feed;

  CustomList(this.feed);

  @override
  _CustomListState createState() => new _CustomListState();

}

class _CustomListState extends State<CustomList> {
  @override
  Widget build(BuildContext context) {
    final taille = MediaQuery.of(context).size.width / 2.5;
    return new ListView.builder(
      itemCount: widget.feed.items.length,
      itemBuilder: (context, i) {
        var item = widget.feed.items[i];
        return new Container(
          child: new Card(
            elevation: 10.0,
            child: new InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (BuildContext context) {
                      return new DetailsPage(item);
                    },
                  ),
                );
              },
              child: new Column(
                children: [
                  padding(),
                  new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new CustomText(
                        (item.dc.creator != null) ? item.dc.creator : ' ',
                        fontSize: 12.0,
                      ),
                      new CustomText(
                        new DateFormatter().formattedDate(item.pubDate),
                        color: Colors.red,
                      ),
                    ],
                  ),
                  padding(),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Card(
                        elevation: 7.5,
                        child: new Container(
                          width: taille,
                          child: new Image.network(
                            item.enclosure.url,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      new Container(
                        width: taille,
                        child: new CustomText(
                          item.title,
                        ),
                      ),
                    ],
                  ),
                  padding(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Padding padding() {
    return new Padding(
      padding: EdgeInsets.only(
        left: 0.0,
        top: 10.0,
        right: 0.0,
        bottom: 0.0
      ),
    );
  }
  
}