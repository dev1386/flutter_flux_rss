import 'package:flutter/material.dart';
import 'package:fluxrss/models/parser.dart';
import 'package:fluxrss/widgets/customGrid.dart';
import 'package:fluxrss/widgets/customList.dart';
import 'package:fluxrss/widgets/loading.dart';
import 'package:webfeed/webfeed.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  RssFeed feed;

  @override
  void initState() {
    super.initState();
    parse();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
        actions: [
          new IconButton(
            icon: new Icon(Icons.refresh),
            onPressed: () {
              setState(() {
                feed = null;
                parse();
              });
            },
          )
        ],
      ),
      body: body(),
    );
  }

  Widget body() {
    if(feed == null) {
      return new Loading();
    } else {
      Orientation orientation = MediaQuery.of(context).orientation;
      if(orientation == Orientation.portrait) {
        return new CustomList(feed);
      } else {
        return new CustomGrid(feed);
      }
    }
  }

  Future<Null> parse() async {
    RssFeed rssFeed = await Parser().loadRss();
    if(rssFeed != null) {
      setState(() {
        feed = rssFeed;
        int i = 0;
        feed.items.forEach((feedItem) {
          print('$i : ${feedItem.enclosure.url}');
          i++;
        });
      });
    }
  }


}