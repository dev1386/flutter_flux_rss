import 'package:flutter/material.dart';
import 'package:fluxrss/widgets/customText.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new CustomText(
        'Chargement en cours...',
        fontStyle: FontStyle.italic,
        fontSize: 30.0,
      ),
    );
  }
  
}