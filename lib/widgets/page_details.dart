import 'package:flutter/material.dart';
import 'package:fluxrss/models/date_formateur.dart';
import 'package:fluxrss/widgets/customText.dart';
import 'package:webfeed/webfeed.dart';

class DetailsPage extends StatelessWidget {

  RssItem item;

  DetailsPage(this.item);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Detail de l'article"),
        centerTitle: true,
      ),
      body: new SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            padding(),
            new CustomText(
              item.title,
              fontSize: 25.0,
              fontStyle: FontStyle.italic,
            ),
            padding(),
            new Card(
              elevation: 7.5,
              child: new Container(
                width: MediaQuery.of(context).size.width / 1.5,
                child: new Image.network(
                  item.enclosure.url,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            padding(),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new CustomText(
                  (item.dc.creator != null) ? item.dc.creator : ' ',
                  fontSize: 12.0,
                ),
                new CustomText(
                  new DateFormatter().formattedDate(item.pubDate),
                  color: Colors.red,
                ),
              ],
            ),
            padding(),
            new CustomText(
              item.description
            ),
          ],
        ),
      ),
    );
  }

  Padding padding() {
    return new Padding(
      padding: EdgeInsets.only(
          left: 0.0,
          top: 10.0,
          right: 0.0,
          bottom: 0.0
      ),
    );
  }

}